function [D,h1]=Plot_Possible_Lam(dim,textflag,markerflag)
if size(dim)==[1 1]
    dim=[dim dim dim];
elseif size(dim)~=[1 3]
    error('didn''t give dimenions of box')
end
counter=1;
for n=0:10
    for i=0:10
        for j=0:10
            D(counter)=prod(dim)/sqrt((n*dim(1)*dim(2)).^2+(i*dim(1)*dim(3)).^2+(j*dim(2)*dim(3)).^2);
            x(counter)=n;
            y(counter)=i;
            z(counter)=j;
            
            counter=counter+1;
            
        end
    end
end

% D=unique(D)

% figure

if ~exist('markerflag','var')


    hold on



    for i=1:length(D)
        if i==find(D==D(i),1)
            if i==1
                h1=plot([0 1],[D(i) D(i)],'k--','LineWidth',2)
            else
                plot([0 1],[D(i) D(i)],'k--','LineWidth',2)
            end
            if exist('textflag','var')
                if textflag==1
                    text(0.5,D(i)+0.1,['D=' num2str(D(i)) ' x=' num2str(x(i)) ' y=' num2str(y(i)) ' z=' num2str(z(i))])
                end
            end
        end
    end
                
elseif markerflag==0



    hold on



    for i=1:length(D)
        if i==find(D==D(i),1)
            if i==1
                h1=plot([0 1],[D(i) D(i)],'k--','LineWidth',2)
            else
                plot([0 1],[D(i) D(i)],'k--','LineWidth',2)
            end
            if exist('textflag','var')
                if textflag==1
                    text(0.5,D(i)+0.1,['D=' num2str(D(i)) ' x=' num2str(x(i)) ' y=' num2str(y(i)) ' z=' num2str(z(i))])
                end
            end
        end
    end
elseif markerflag==1
    
    hold on

    for i=1:length(D)
        if i==find(D==D(i),1)
            if i==1
                h1=plot(D(i),0,'ks')
            else
                plot(D(i),0,'ks')
            end
            if exist('textflag','var')
                if textflag==1
                    text(0.5,D(i)+0.1,['D=' num2str(D(i)) ' x=' num2str(x(i)) ' y=' num2str(y(i)) ' z=' num2str(z(i))])
                end
            end
        end
    end
end
     



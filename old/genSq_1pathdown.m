%searches through folders 1 path down and generates S(Q) and g(r) plots for
%AA, CC, and DD
%%%%DEPRECATED

path='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Newruns_4_18_18/';
paths=dir(path);
for i=3:length(paths)
    if paths(i).isdir==1
        newpath=[path paths(i).name '/'];
        files=dir(newpath);
        for j=3:length(files)
            startIndex = regexp(files(j).name,'PCND_Nano.+start.xml');
            if ~isempty(startIndex)
                xmlname=files(j).name;
                dcdname=[xmlname(1:end-9) 'PCND1traj.dcd'];
                if exist([newpath dcdname], 'file') == 2
                    h=read_dcdheader([newpath dcdname]);
                    nsteps=h.NSET;
                    Inputnames={[newpath xmlname],[newpath dcdname]};
                    OutFileAA=[newpath xmlname(1:end-9) '_grAA.dat'];
                    OutFileCC=[newpath xmlname(1:end-9) '_grCC.dat'];
                    OutFileDD=[newpath xmlname(1:end-9) '_grDD.dat'];
                    if nsteps>=100
                        [rAA,grAA] = rdf('Inputnames',Inputnames,'Selection1','name A','Selection2','name A','Delta',0.1,'Rmax',100,'FirstStep',nsteps-100,'LastStep',nsteps-1,'StepSize',1,'OutFile',OutFileAA);
                        [rCC,grCC] = rdf('Inputnames',Inputnames,'Selection1','name C','Selection2','name C','Delta',0.1,'Rmax',100,'FirstStep',nsteps-100,'LastStep',nsteps-1,'StepSize',1,'OutFile',OutFileCC);
                        [rDD,grDD] = rdf('Inputnames',Inputnames,'Selection1','name D','Selection2','name D','Delta',0.1,'Rmax',100,'FirstStep',nsteps-100,'LastStep',nsteps-1,'StepSize',1,'OutFile',OutFileDD);
                       
                        %%Plotting
                        % PlottingAA
                        figure_AA_gr = plot(rAA, grAA, 'r', 'LineWidth', 2);
                        title({'Radial Pair Distribution Function g(r)', [xmlname(1:end-9) ' : A-A']});
                        axis([0 max(rAA) 0 max(grAA)+0.5]);
                        xlabel('r');
                        ylabel('g(r)');
                        grid on;
                        % Saving
                        fpath_AA_gr = [newpath xmlname(1:end-9) 'g(r)_A-A.jpg'];
                        saveas(figure_AA_gr, fpath_AA_gr);
                        
                        % PlottingCC
                        figure_CC_gr = plot(rCC, grCC, 'r', 'LineWidth', 2);
                        title({'Radial Pair Distribution Function g(r)', [xmlname(1:end-9) ' : C-C']});
                        axis([0 max(rCC) 0 max(grCC)+0.5]);
                        xlabel('r');
                        ylabel('g(r)');
                        grid on;
                        % Saving
                        fpath_CC_gr = [newpath xmlname(1:end-9) 'g(r)_C-C.jpg'];
                        saveas(figure_CC_gr, fpath_CC_gr);
                        
                        % PlottingDD
                        figure_DD_gr = plot(rDD, grDD, 'r', 'LineWidth', 2);
                        title({'Radial Pair Distribution Function g(r)', [xmlname(1:end-9) ' : D-D']});
                        axis([0 max(rDD) 0 max(grDD)+0.5]);
                        xlabel('r');
                        ylabel('g(r)');
                        grid on;
                        % Saving
                        fpath_DD_gr = [newpath xmlname(1:end-9) 'g(r)_D-D.jpg'];
                        saveas(figure_DD_gr, fpath_DD_gr);
                        
                        close all
                        fclose all
                        
                        clear S_AA S_CC S_DD Sq_AA Sq_CC Sq_DD del_r_AA del_r_CC del_r_DD
                        %% Calculation of S(q)
                        %% A-A
                        rmax_AA = rAA(end);
                        ru0_AA = 1;
                        n1 = 1;
                        for m1 = 1:length(rAA)
                            if m1 == 1
                                del_r_AA(1,1) = rAA(1,1) - 0;
                            else
                                del_r_AA(m1,1) = rAA(m1,1) - rAA(m1-1,1);
                            end
                        end
                        qs_AA = 0.001:0.001:10;
                        for q1 = qs_AA
                            S_AA = 4.*pi.*ru0_AA.*rAA.*((sin(q1.*rAA))./q1).*(grAA-1).*(sin((pi.*rAA)./rmax_AA)./((pi.*rAA)/rmax_AA));
                            Sq_AA(n1,1) = (sum(S_AA(1:end).*del_r_AA))-1;
                            n1 = n1 + 1;
                        end
                        
                        %%C-C
                        rmax_CC = rCC(end);
                        ru0_CC = 1;
                        n1 = 1;
                        for m1 = 1:length(rCC)
                            if m1 == 1
                                del_r_CC(1,1) = rCC(1,1) - 0;
                            else
                                del_r_CC(m1,1) = rCC(m1,1) - rCC(m1-1,1);
                            end
                        end
                        qs_CC = 0.001:0.001:10;
                        for q1 = qs_CC
                            S_CC = 4.*pi.*ru0_CC.*rCC.*((sin(q1.*rCC))./q1).*(grCC-1).*(sin((pi.*rCC)./rmax_CC)./((pi.*rCC)/rmax_CC));
                            Sq_CC(n1,1) = (sum(S_CC(1:end).*del_r_CC))-1;
                            n1 = n1 + 1;
                        end
                        
                        %%D-D
                        rmax_DD = rDD(end);
                        ru0_DD = 1;
                        n1 = 1;
                        for m1 = 1:length(rDD)
                            if m1 == 1
                                del_r_DD(1,1) = rDD(1,1) - 0;
                            else
                                del_r_DD(m1,1) = rDD(m1,1) - rDD(m1-1,1);
                            end
                        end
                        qs_DD = 0.001:0.001:10;
                        for q1 = qs_DD
                            S_DD = 4.*pi.*ru0_DD.*rDD.*((sin(q1.*rDD))./q1).*(grDD-1).*(sin((pi.*rDD)./rmax_DD)./((pi.*rDD)/rmax_DD));
                            Sq_DD(n1,1) = (sum(S_DD(1:end).*del_r_DD))-1;
                            n1 = n1 + 1;
                        end
                        
                        
                        %% Calculatin of q*
                        % A-A
                        z_AA = find(Sq_AA == max(Sq_AA));
                        q_star_AA = qs_AA(z_AA);
                        % C-C
                        z_CC = find(Sq_CC == max(Sq_CC));
                        q_star_CC = qs_CC(z_CC);
                        % A-A
                        z_DD = find(Sq_DD == max(Sq_DD));
                        q_star_DD = qs_DD(z_DD);
                        
                        %%% Plotting and Saving S(q)-q Graphs
                        %% A-A
                        % Plotting
                        figure_AA_Sq = plot(qs_AA, Sq_AA, 'r', 'LineWidth', 2);
                        text(q_star_AA, Sq_AA(z_AA), ['\leftarrow q* = ' num2str(q_star_AA)]);
                        title({'Structure Factor S(q)', [xmlname(1:end-9) ' : A-A']});
                        xlabel('q');
                        ylabel('S(q)');
                        grid on;
                        % Saving
                        fpath_AA_Sq = [newpath xmlname(1:end-9) 'S(q)_A-A.jpg'];
                        saveas(figure_AA_Sq, fpath_AA_Sq);
                        %% C-C
                        % Plotting
                        figure_CC_Sq = plot(qs_CC, Sq_CC, 'r', 'LineWidth', 2);
                        text(q_star_CC, Sq_CC(z_CC), ['\leftarrow q* = ' num2str(q_star_CC)]);
                        title({'Structure Factor S(q)', [xmlname(1:end-9) ' : C-C']});
                        xlabel('q');
                        ylabel('S(q)');
                        grid on;
                        % Saving
                        fpath_CC_Sq = [newpath xmlname(1:end-9) 'S(q)_C-C.jpg'];
                        saveas(figure_CC_Sq, fpath_CC_Sq);
                        %% D-D
                        % Plotting
                        figure_DD_Sq = plot(qs_DD, Sq_DD, 'r', 'LineWidth', 2);
                        text(q_star_DD, Sq_DD(z_DD), ['\leftarrow q* = ' num2str(q_star_DD)]);
                        title({'Structure Factor S(q)', [xmlname(1:end-9) ' : D-D']});
                        xlabel('q');
                        ylabel('S(q)');
                        grid on;
                        % Saving
                        fpath_DD_Sq = [newpath xmlname(1:end-9) 'S(q)_D-D.jpg'];
                        saveas(figure_DD_Sq, fpath_DD_Sq);
                        
                        close all
                        fclose all
                    end
                end
            end
        end
    end
end

                
                

function [Rg2,S,V,D] = GyTens(pos)
pos=pos-COM(pos);

S=zeros(3);
for m=1:3
    for n=1:3
        stally=0;
        for i=1:size(pos,1)
            stally=stally+pos(i,m)*pos(i,n);
        end
        stally=stally/size(pos,1);
        S(m,n)=stally;
    end
end
[V,D]=eig(S);
D=[D(1,1),D(2,2),D(3,3)];
Rg2=sum(D);
end
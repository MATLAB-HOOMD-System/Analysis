function [r,gr,grave]=rdf_M(pos,dim,deltar,rmax,plotflag)
Edges=(0:deltar:rmax);
Centers=Edges(1:end-1)+deltar;
parfor i=1:size(pos,3)
%     if mod(i,100)==0
%         i
%     end
    d=distanceimage(pos(:,:,i),dim(i,:),0);
    
    H=histcounts(d,Edges);
    
    H=H./((Edges(2:end).^3-Edges(1:end-1).^3)*4*pi()/3);
    rho=size(pos,1)/(dim(i,1)*dim(i,2)*dim(i,3));
    H=H/rho;
    H=2*H/size(pos,1);
    %         plot(Centers,H)
    gr(i,:)=H;
end
grave=mean(gr,1);
r=Centers;
if exist('plotflag','var')
    if plotflag==1
        figure
        plot(r,grave)
    end
end
end
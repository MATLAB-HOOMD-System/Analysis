close all
clear all
clear D D2 Dx Dy Dz D2z D2x D2y A Apos dist dist1 meandist
path='/home/andrew/simulationtesting/NanoDiff/SingleParticleDiffusion/';
% clear all

for n=1:9
    clear D Dx Dy Dz A Apos
    xmlname=[path 'NanoDiff_start.xml'];
    n
    dcdname=[path 'NanoDiff' num2str(n) '_PCND1traj.dcd']
    
    system=Extract_XML_DCD(xmlname,dcdname,0,0);

    Apos=system.pos(1,:,1500:1000:4600);

% Apos=Apos(1,:,1:10);


    for i=2:size(Apos,3)
        D(i)=sqrt((Apos(1,1,i)-Apos(1,1,i-1))^2+(Apos(1,2,i)-Apos(1,2,i-1))^2+(Apos(1,3,i)-Apos(1,3,i-1))^2);
        Dx(i)=((Apos(1,1,i)-Apos(1,1,i-1)));
        Dy(i)=((Apos(1,2,i)-Apos(1,2,i-1)));
        Dz(i)=((Apos(1,3,i)-Apos(1,3,i-1)));

    %     A=permute(Apos,[2 3 1])';
    %     plot3(A(:,1),A(:,2),A(:,3),'k-')
    % %     axis([-20 20 -20 20 -20 20])
    %     pause(0.01)

        if Dx(i)>system.dim(1)/2
            Apos(1,1,i:end)=Apos(1,1,i:end)-system.dim(1);
        elseif Dx(i)<-system.dim(1)/2
            Apos(1,1,i:end)=Apos(1,1,i:end)+system.dim(1);
        end

        if Dy(i)>system.dim(2)/2
            Apos(1,2,i:end)=Apos(1,2,i:end)-system.dim(2);
        elseif Dy(i)<-system.dim(2)/2
            Apos(1,2,i:end)=Apos(1,2,i:end)+system.dim(2);
        end

        if Dz(i)>system.dim(3)/2
            Apos(1,3,i:end)=Apos(1,3,i:end)-system.dim(3);
        elseif Dz(i)<-system.dim(3)/2
            Apos(1,3,i:end)=Apos(1,3,i:end)+system.dim(3);
        end

        D2(i)=sqrt((Apos(1,1,i)-Apos(1,1,i-1))^2+(Apos(1,2,i)-Apos(1,2,i-1))^2+(Apos(1,3,i)-Apos(1,3,i-1))^2);
        D2x(i)=((Apos(1,1,i)-Apos(1,1,i-1)));
        D2y(i)=((Apos(1,2,i)-Apos(1,2,i-1)));
        D2z(i)=((Apos(1,3,i)-Apos(1,3,i-1)));



    end

    % close all 
    % plot(D2)
    A=permute(Apos,[2 3 1])';
%     plot3(A(:,1),A(:,2),A(:,3),'k-')


    for i=2:size(A,1)
        dist(i,n)=sqrt((A(i,1)-A(1,1))^2+(A(i,2)-A(1,2))^2+(A(i,3)-A(1,3))^2);
    end
end

dist2=dist(1:end-1,:); % remove last line which includes a 0

% dist2=dist2.^2;
meandist=mean(dist2.^2,2);

close all
% plot(timemeandist,'r')
f=figure
f.Position=[500 500 560 420]
hold on

time=(0:size(A,1)-2)*10000;
S=polyfit(time,meandist',1);
dlm = fitlm(time,meandist','y~x1-1');
S(1)=dlm.Coefficients.Estimate;
S(2)=0;
plot(time,meandist,'r');
plot(time,S(2)+S(1).*time,'b');

set(gca,'FontSize',12)
set(gcf,'Color','w')
% set(gcf,'Units','inches')
% set(gcf,'Position',[5 5 3.5 3])
set(gcf,'Position',[2003         906      400*1.1666      400])
xlabel('timesteps')
ylabel('MSD')
box on


D=S(1)/6;

TDIFF=((14.41/4)^2)/(6*D)

% export_fig -r200 -a2 MSD.png

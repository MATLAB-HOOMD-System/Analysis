close all
clear all
clear D D2 Dx Dy Dz D2z D2x D2y A Apos dist dist1 meandist
% clear all
system=Extract_XML_DCD("NanoDiff_start.xml","NanoDiff1_PCND1traj.dcd",0,0);

Apos=system.pos(1,:,:);

% Apos=Apos(1,:,1:10);


for i=2:size(Apos,3)
    D(i)=sqrt((Apos(1,1,i)-Apos(1,1,i-1))^2+(Apos(1,2,i)-Apos(1,2,i-1))^2+(Apos(1,3,i)-Apos(1,3,i-1))^2);
    Dx(i)=((Apos(1,1,i)-Apos(1,1,i-1)));
    Dy(i)=((Apos(1,2,i)-Apos(1,2,i-1)));
    Dz(i)=((Apos(1,3,i)-Apos(1,3,i-1)));
    
%     A=permute(Apos,[2 3 1])';
%     plot3(A(:,1),A(:,2),A(:,3),'k-')
% %     axis([-20 20 -20 20 -20 20])
%     pause(0.01)
    
    if Dx(i)>system.dim(1)/2
        Apos(1,1,i:end)=Apos(1,1,i:end)-system.dim(1);
    elseif Dx(i)<-system.dim(1)/2
        Apos(1,1,i:end)=Apos(1,1,i:end)+system.dim(1);
    end
    
    if Dy(i)>system.dim(2)/2
        Apos(1,2,i:end)=Apos(1,2,i:end)-system.dim(2);
    elseif Dy(i)<-system.dim(2)/2
        Apos(1,2,i:end)=Apos(1,2,i:end)+system.dim(2);
    end
    
    if Dz(i)>system.dim(3)/2
        Apos(1,3,i:end)=Apos(1,3,i:end)-system.dim(3);
    elseif Dz(i)<-system.dim(3)/2
        Apos(1,3,i:end)=Apos(1,3,i:end)+system.dim(3);
    end
    
    D2(i)=sqrt((Apos(1,1,i)-Apos(1,1,i-1))^2+(Apos(1,2,i)-Apos(1,2,i-1))^2+(Apos(1,3,i)-Apos(1,3,i-1))^2);
    D2x(i)=((Apos(1,1,i)-Apos(1,1,i-1)));
    D2y(i)=((Apos(1,2,i)-Apos(1,2,i-1)));
    D2z(i)=((Apos(1,3,i)-Apos(1,3,i-1)));
    
    
    
end

% close all 
% plot(D2)
A=permute(Apos,[2 3 1])';
plot3(A(:,1),A(:,2),A(:,3),'k-')


for i=2:size(A,1)
    dist1(i)=sqrt((A(i,1)-A(1,1))^2+(A(i,2)-A(1,2))^2+(A(i,3)-A(1,3))^2);
end

dist=zeros(size(A,1),size(A,1));

for j=1:size(A,1)
    j
    for i=j:size(A,1)
        dist(j,i)=sqrt((A(i,1)-A(j,1))^2+(A(i,2)-A(j,2))^2+(A(i,3)-A(j,3))^2);
    end
end

for k=2:size(dist,1)
    meandist(k)=mean(dist(dist(:,k)~=0,k).^2);
end

close all
plot(dist1.^2,'k')
hold on
plot(meandist,'r')

time=(0:size(A,1)-1)*10000;
S=polyfit(time,meandist,1);
D=S(1)/6;

TDIFF=(3^2)/(6*D)
% for i=1:length(D)
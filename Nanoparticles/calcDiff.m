clear all
close all
loadvals=1;
if loadvals==1
Path='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Refine_PCND_Nano/HomopolymerDiffusion/';
xml='Make_PCNDNano110_r2_N18NA9_nG60_start.xml';
files=dir(Path);
counter=1;
for i=3:length(files)
    if files(i).name(end-13:end)=='_PCND1traj.dcd'
        tempname=files(i).name;
        Xi1=strfind(tempname,'XiNano');
        Xi2=strfind(tempname,'tNano');
        Xi3=strfind(tempname,'XiPoly');
        Xi4=strfind(tempname,'tPoly');
        Xi5=strfind(tempname,'_10mil');
        XiPolyTEMP=files(i).name(Xi3+6:Xi4-2);
        tNanoTEMP=(files(i).name(Xi2+5:Xi3-2));
        XiNanoTEMP=(files(i).name(Xi1+6:Xi2-2));
        tPolyTEMP=(files(i).name(Xi4+5:Xi5-2));
%         if strcmp(XiNanoTEMP,'0')==1 && strcmp(tPolyTEMP,'100000')==1
            dcd{counter}=files(i).name;
            labels{counter}=[files(i).name(Xi1:Xi5-1) ];
            XiNano(counter)=str2num(files(i).name(Xi1+6:Xi2-2));
            tNano(counter)=str2num(files(i).name(Xi2+5:Xi3-2));
            tPoly(counter)=str2num(files(i).name(Xi4+5:Xi5-1));
            XiPoly(counter)=str2num(files(i).name(Xi3+6:Xi4-2));
            counter=counter+1;
%         end
    end
end

% dcd={'PCND_Nano_XiNano0.01_tNano300000_XiPoly0_tPoly0_10milHOMO1_PCND1traj.dcd','PCND_Nano_XiNano0.005_tNano300000_XiPoly0_tPoly0_10milHOMO1_PCND1traj.dcd','PCND_Nano_XiNano0.008_tNano300000_XiPoly0_tPoly0_10milHOMO1_PCND1traj.dcd'};
% labels={'Xi0.1, Tau1e7','Xi0.005, Tau1e7','Xi0.08, Tau1e7'};
% close all
disp(['Number of Files = ' num2str(counter-1)])
colors=lines(length(dcd));

for k=1:length(dcd)
    k
    counter-1
    stepsize=10000;
    system=Extract_XML_DCD([Path xml],[Path dcd{k}],0,0)

    system=strip_beads(0,system,'A')
    system.pos=packing_all(system.pos,system.bonds,system.dim,[0 0 0])

    MSD_t=MSD(system.pos);

%     close all
    plot(1:1000,MSD_t,'-','Color',colors(k,:))
    hold on
    x=1:1000;
    p=polyfit(x,MSD_t(x)/6,1);
    fitMSD(k)=p(1);
    plot(x,x.*p(1)+p(2),'--','Color',colors(k,:))
%     pause(0.001)
    if k==1
        legtext={labels{1},num2str(p(1))};
    else
        legtext{end+1}=labels{k};
        legtext{end+1}=num2str(p(1));
    end
end
legend(legtext)

save('NanoDiff.mat')
else
    load('NanoDiff.mat')
end
%tNano=300000
%XiNano=0.008
%tPoly=1000000
%XiPoly=0.1

vars={'tNano','XiNano','tPoly','XiPoly'};
vals=[300000,0.008,1000000,0.1];

for i=1:length(vals)
    if ~isempty(strfind(vars{i},'Nano'))
        if strcmp(vars{i},'tNano')  %tNano=300000 XiNano varies
            xlab='XiNano';
            indices=tNano==vals(1);
            Xis=XiNano(indices);
            MSDs=fitMSD(indices);
            figure
            plot(Xis,MSDs,'ok')
            f = fit(Xis',MSDs','power2')
            hold on
            x=min(Xis):0.001:max(Xis);
            plot(x,f.a.*(x.^(f.b))+f.c)
            xlabel('XiNano')
            ylabel('D')
            fitstr=['y=' num2str(f.a) 'x^{' num2str(f.b) '}+' num2str(f.c)]
            text(0.004,80,fitstr)
        else                  %XiNano=0.008  tNano Varies
            xlab='tNano'; 
            indices=XiNano==vals(2);
            ts=tNano(indices);
            MSDs=fitMSD(indices);
            figure
            plot(ts,MSDs,'ok')
            f = fit(ts',MSDs','power2')
            hold on
            x=min(ts):1:max(ts);
            plot(x,f.a.*(x.^(f.b))+f.c)
            xlabel('tNano')
            ylabel('D')
            fitstr=['y=' num2str(f.a) 'x^{' num2str(f.b) '}+' num2str(f.c)]
            text(2,400,fitstr)
        end
    else
        if strcmp(vars{i},'tPoly')
            xlab='XiPoly';
            indices=tPoly==vals(3);
            Xis=XiPoly(indices);
            MSDs=fitMSD(indices);
            figure
            plot(Xis,MSDs,'ok')
            f = fit(Xis',MSDs','power2')
            hold on
            x=min(Xis):0.001:max(Xis);
            plot(x,f.a.*(x.^(f.b))+f.c)
            xlabel('XiPoly')
            ylabel('D')
            fitstr=['y=' num2str(f.a) 'x^{' num2str(f.b) '}+' num2str(f.c)]
            text(0.04,14,fitstr)
        else
            xlab='tPoly';
            indices=XiPoly==vals(4);
            ts=tPoly(indices);
            MSDs=fitMSD(indices);
            figure
            plot(ts,MSDs,'ok')
            set(gca,'YLim', [0 60])
            f = fit(ts',MSDs','power2')
            hold on
            x=min(ts):1:max(ts);
            plot(x,f.a.*(x.^(f.b))+f.c)
            xlabel('tPoly')
            ylabel('D')
            fitstr=['y=' num2str(f.a) 'x^{' num2str(f.b) '}+' num2str(f.c)]
            text(1.5e7,5,fitstr)
        end
    end
end


% figure
% plot(XiPoly,fitMSD,'ok')
% f = fit(XiPoly',fitMSD'/6,'power2')
% hold on
% x=min(XiPoly):0.001:max(XiPoly);
% plot(x,f.a.*(x.^(f.b))+f.c)
% xlabel('XiPoly')
% ylabel('D')

% colors=lines(110);
% for i=1:110
%     scatter3(system.pos(i,1,:),system.pos(i,2,:),system.pos(i,3,:),'MarkerEdgeColor',colors(i,:))
%     hold on
% end
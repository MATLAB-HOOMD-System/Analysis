close all
clear all
loadPCND=0;
loadacf=0;
Plot=1;
corrtype=1;

if loadPCND==0
%% calc PCND

%%% Looks like all the bbove was not needed

tauPolys=[20 200 2000 20000];
% tauPolys=200;
for N=1:length(tauPolys)


XiNano=1;
% XiNano=XiNanos(N);
tNano=tauPolys(N);
XiPoly=1;
% XiPoly=XiPolys(N);
tPoly=tauPolys(N);
totaltime=3e4;
numruns=10;

%% repeats
parfor k=1:numruns
    if mod(k,10)==0
        disp('k=')
        disp(k)
        disp('N=')
        disp(N)
    end


%% initialize values
i=1;
e0x=XiNano*sqrt(-2*log(rand()))*cos(2*pi()*rand());
e0y=XiNano*sqrt(-2*log(rand()))*cos(2*pi()*rand());
e0z=XiNano*sqrt(-2*log(rand()))*cos(2*pi()*rand());
E=exp(-1/tNano);
E2=E*E;
Et=exp(-1/tPoly);
Et2=Et*Et;
ex=zeros(1,totaltime);
ey=zeros(1,totaltime);
ez=zeros(1,totaltime);
e=zeros(totaltime,3);
Nanomag=zeros(1,totaltime);
ex(1)=e0x;
ey(1)=e0y;
ez(1)=e0z;
e(1,:)=[e0x e0y e0z];
Nanomag(i)=sqrt(ex(i)^2+ey(i)^2+ez(i)^2);

%initialize polymer arm forces
Ptot=zeros(totaltime,3);
Ptot(1,:)=e(1,:);
exP=zeros(60,totaltime);
eyP=zeros(60,totaltime);
ezP=zeros(60,totaltime);
Polymag=zeros(1,totaltime);

for j=1:60
    exP(j,i)=XiPoly*sqrt(-2*log(rand()))*cos(2*pi()*rand());
    eyP(j,i)=XiPoly*sqrt(-2*log(rand()))*cos(2*pi()*rand());
    ezP(j,i)=XiPoly*sqrt(-2*log(rand()))*cos(2*pi()*rand());
    Ptot(i,:)=Ptot(i)+18*[exP(j,i) eyP(j,i) ezP(j,i)];
end





% profile on
%% Through Time
for i=2:totaltime
    if mod(i,1e6)==0
        disp(i)
    end
    
%     Nanomag(i)=ex(i);
    %% Through 60 polymer arms
    Ptot(i)=e(i)*0;
    oldnumsx=exP(:,i-1);
    oldnumsy=eyP(:,i-1);
    oldnumsz=ezP(:,i-1);
    for j=1:60
        h=XiPoly*sqrt(-2*(1-Et2)*log(rand()))*cos(2*pi()*rand());
        exP(j,i)=Et*oldnumsx(j)+h;
        h=XiPoly*sqrt(-2*(1-Et2)*log(rand()))*cos(2*pi()*rand());
        eyP(j,i)=Et*oldnumsy(j)+h;
        h=XiPoly*sqrt(-2*(1-Et2)*log(rand()))*cos(2*pi()*rand());
        ezP(j,i)=Et*oldnumsz(j)+h;
    end
    
    Ptot(i,:)=Ptot(i)+18*[sum(exP(:,i)) sum(eyP(:,i)) sum(ezP(:,i))];
    Ptot(i,:)=Ptot(i,:)/(60*18);
    Polymag(i)=sqrt(Ptot(i,1)^2+Ptot(i,2)^2+Ptot(i,3)^2);
end
    
% Pave(k)=mean(Polymag);
% Nave(k)=mean(Nanomag);
Pave(k)=sqrt(sum(Polymag.^2)/length(Polymag));
% Nave(k)=sqrt(sum(Nanomag.^2)/length(Nanomag));
Polymagout(k,:)=Polymag;
% Nanomagout(k,:)=Nanomag;
% exout(:,k)=ex;
exPout(:,k,N)=Ptot(:,1);

end

% XiNano
XiPoly
% mNave=mean(Nave)
mPave=mean(Pave)
% saveaves(N,:)=[mNave mPave]

% caveN=cum_ave(Nave);
caveP=cum_ave(Pave);

% plot(1:numruns,caveN)
% hold on
% plot(1:numruns,caveP)

%% Plotting
% plot(1:totaltime,Nanomag)
% hold on
% plot(1:totaltime,Polymag)

% profile viewer

end


elseif loadPCND==1
load('genPCND.mat')
end


%% calc autocorr and fit
if loadacf==0
    for N=1:length(tauPolys)
        parfor i=1:numruns
            i
        %     NanoA(:,i)=acf(exout(1:10:end,i),2000,0,corrtype);
            PolyA(:,i,N)=acf(exPout(1:10:end,i,N),2000,0,corrtype);
        end
    end
% NanoAmean=mean(NanoA,2);
for N=1:length(tauPolys)
    PolyAmean(:,N)=mean(PolyA(:,:,N),2);
end
% save('genacf.mat','NanoA','PolyA','PolyAmean','NanoAmean')
elseif loadacf==1
% load('genacf.mat')
end


if Plot==1
for N=1:length(tauPolys)
%     plot(1:2000,NanoAmean,'r')
%     hold on
    plot(1:2000,PolyAmean(:,N))%,'b')
    hold on
%     plot(1:2000,exp((-(1:2000))/100),'k')
%     plot(1:2000,PolyAmean/PolyAmean(1),'b--')
    set(gca,'YScale','log')
    axis([0 400 0.001 1])
    xlabel('time')
    ylabel('Autocorrelation')
    for i=1:length(tauPolys);
        legstr{i}=num2str(tauPolys(i));
    end
    legend(legstr)
    set(gcf,'color','w')
    
%     figure
%     hold on
%     for i=1:size(NanoA,2)
%         plot(1:2000,NanoA(:,i))
%         hold on
%     end

end
end





function output=cum_ave(input);
    for i=1:length(input)
        output(i)=sum(input(1:i))/i;
    end
end






% 
% 
% 
% 
% 
% 
% figure
% for i=1:length(system.attype)
%     if system.attype(i)=='A'
%         colors(i,:)=[0 0 0];
%         size(i)=100;
%     elseif system.attype(i)=='B'
%         colors(i,:)=[1 0 0];
%         size(i)=30;
%     elseif system.attype(i)=='C'
%         colors(i,:)=[0 0 1];
%         size(i)=10;
%     elseif system.attype(i)=='D'
%         colors(i,:)=[0 1 0];
%         size(i)=10;
%     end
% end
% scatter3(system.pos(:,1),system.pos(:,2),system.pos(:,3),size,colors)

% export_fig -r200 -a2 PCNDNano_1.png

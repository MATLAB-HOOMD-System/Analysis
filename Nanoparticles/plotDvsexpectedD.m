clear all
close all
numberline=1;


filename='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Results Summary.xlsx';
sheet='FixedPCND, refinePCND';
xlRange='T27:AD46';
xlRange2='AH27:AR46';


if numberline~=1

    Ds = xlsread(filename,sheet,xlRange)

    type= xlsread(filename,sheet,xlRange2)

    figure 
    hold on
    counter=0;
    for i=1:size(Ds,1)
        for j=1:size(Ds,2)
            if type(i,j)==1
                counter=counter+1;
                D(counter)=Ds(i,j);
            end
        end
    end

    for i=1:length(D)
        if i==1
            h2=plot([0 1],[D(i) D(i)],'LineWidth',2,'Color','r')
        else
            plot([0 1],[D(i) D(i)],'LineWidth',2,'Color','r')
        end
    end

    [Dexp, h1]=Plot_Possible_Lam(35.11,0)

    set(gca,'FontSize',12)
    set(gcf,'Color','w')
    set(gcf,'Position',[2003         906         834         741])
    box on

    A=gca;
    F=gcf;


    A.XTick=[];
    A.XTickLabel={};
    ylabel('{\it D} or {\it P}')

    legend([h1 h2],{'Possible Domain Size {\it P}','Meausured Domain Size {\it D}'});

    axis([0 1 5 22])

    set(gcf,'Position',[2003         906      400*1.1666      400])
elseif numberline==1
    Ds = xlsread(filename,sheet,xlRange)

    type= xlsread(filename,sheet,xlRange2)

    figure 
    hold on
    counter=0;
    for i=1:size(Ds,1)
        for j=1:size(Ds,2)
            if type(i,j)==1
                counter=counter+1;
                D(counter)=Ds(i,j);
            end
        end
    end

    for i=1:length(D)
        if i==1
            h2=plot(D(i),0,'ro')
        else
            plot(D(i),0,'ro')
        end
    end

    [Dexp, h1]=Plot_Possible_Lam(35.11,0,1)

h1=figure(1);clf
subplot(4,1,1);
hold on
xlim([5,22]);ylim([-1,1])

%arrow
[arrowX,arrowY]=dsxy2figxy([5,22],[0,0]);
annotation('arrow',arrowX,arrowY)

%circles
x=[D];
H2=plot(x,zeros(size(x)),'ro','markersize',10)


%crosses
x=[Dexp];
H1=plot(x,0,'kx','markersize',10)



%pipes
p=5:5:20;
text(p,zeros(1,size(p,2)),'$$\vert$$','interpreter','latex')

%text
text(p,(-0.5)*ones(size(p)),{'5','10','15','20'},'interpreter','latex')

axis off
print('-depsc','arrowFigure')
set(gcf,'Color','w')
text(13,-1,'{\it D} or {\it P}')

leg=legend([H1(1),H2(1)],{'Allowable Domain Size','Measured Domain Size'})
leg.FontSize=10;
end


%{'0','5','10','15','20','25','30','35','40'}


% export_fig -r200 -a2 DvsP.png
    
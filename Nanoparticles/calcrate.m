close all
totalnum=4;
vals=[4.02E+06
4.00E+06
9.40E+06
3.60E+06
];
vals=sort(vals);
f=fittype(@(a,b,x) exp(a*(x-b).*heaviside(x-b)));
method=1;
if method==1
    A=[0:10000:1e7];
    B=ones(size(A));

    for i=1:length(vals)
        B(A>=vals(i))=B(A>=vals(i))-1/totalnum;
    end
elseif method==2
    A=[0 vals'];
    clear B
    B(1)=1;
    for i=2:length(vals)+1
        B(i)=B(i-1)-1/totalnum;
    end
end

[F,Fg,Fo]=fit(A',B',f,'StartPoint',[-1e-7,2e6],'Algorithm','Levenberg-Marquardt','Robust','LAR');
B2=exp(F.a*(A-F.b).*heaviside(A-F.b));

plot(A,B,'ks')
hold on
plot(A,B2,'r')
F
Fg
time=-1/F.a+F.b
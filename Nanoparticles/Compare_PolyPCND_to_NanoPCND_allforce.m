close all
clear all

%% calc PCND

%%% Looks like all the bbove was not needed

XiNanos=[0.002
0.005
0.006
0.008
0.01
0.012
0.014
0.016
0.018
0.02
0.022
0.024
0.026
0.028
0.03
0.032
0.034
0.036
0.038
0.04]
XiPolys=[1
0.6
0.3
0.2
0.16
0.14
0.12
0.1
0.08
0.06
0.04
0.02
0.014
0.010
0
0
0
0
0
0
];
for N=1:length(XiNanos)


XiNano=0.01;
XiNano=XiNanos(N);
tNano=10000;
XiPoly=0.1;
XiPoly=XiPolys(N);
tPoly=10000;
totaltime=1e5;
numruns=8;

%% repeats
parfor k=1:numruns
    disp('k=')
    disp(k)


%% initialize values
i=1;
e0x=XiNano*sqrt(-2*log(rand()))*cos(2*pi()*rand());
e0y=XiNano*sqrt(-2*log(rand()))*cos(2*pi()*rand());
e0z=XiNano*sqrt(-2*log(rand()))*cos(2*pi()*rand());
E=exp(-1/tNano);
E2=E*E;
Et=exp(-1/tPoly);
Et2=Et*Et;
ex=zeros(1,totaltime);
ey=zeros(1,totaltime);
ez=zeros(1,totaltime);
e=zeros(totaltime,3);
Nanomag=zeros(1,totaltime);
ex(1)=e0x;
ey(1)=e0y;
ez(1)=e0z;
e(1,:)=[e0x e0y e0z];
Nanomag(i)=sqrt(ex(i)^2+ey(i)^2+ez(i)^2);

%initialize polymer arm forces
Ptot=zeros(totaltime,3);
Ptot(1,:)=e(1,:);
exP=zeros(60,totaltime);
eyP=zeros(60,totaltime);
ezP=zeros(60,totaltime);
Polymag=zeros(1,totaltime);

for j=1:60
    exP(j,i)=XiPoly*sqrt(-2*log(rand()))*cos(2*pi()*rand());
    eyP(j,i)=XiPoly*sqrt(-2*log(rand()))*cos(2*pi()*rand());
    ezP(j,i)=XiPoly*sqrt(-2*log(rand()))*cos(2*pi()*rand());
    Ptot(i,:)=Ptot(i)+18*[exP(j,i) eyP(j,i) ezP(j,i)];
end





% profile on
%% Through Time
for i=2:totaltime
    if mod(i,1e5)==0
        disp(i)
    end
    
    h=XiNano*sqrt(-2*(1-E2)*log(rand()))*cos(2*pi()*rand());
    ex(i)=E*ex(i-1)+h;
    h=XiNano*sqrt(-2*(1-E2)*log(rand()))*cos(2*pi()*rand());
    ey(i)=E*ey(i-1)+h;
    h=XiNano*sqrt(-2*(1-E2)*log(rand()))*cos(2*pi()*rand());
    ez(i)=E*ez(i-1)+h;
    e(i,:)=[ex(i) ey(i) ez(i)];
    Nanomag(i)=sqrt(ex(i)^2+ey(i)^2+ez(i)^2);
    %% Through 60 polymer arms
    Ptot(i)=e(i)*0;
    oldnumsx=exP(:,i-1);
    oldnumsy=eyP(:,i-1);
    oldnumsz=ezP(:,i-1);
    for j=1:60
        h=XiPoly*sqrt(-2*(1-Et2)*log(rand()))*cos(2*pi()*rand());
        exP(j,i)=Et*oldnumsx(j)+h;
        h=XiPoly*sqrt(-2*(1-Et2)*log(rand()))*cos(2*pi()*rand());
        eyP(j,i)=Et*oldnumsy(j)+h;
        h=XiPoly*sqrt(-2*(1-Et2)*log(rand()))*cos(2*pi()*rand());
        ezP(j,i)=Et*oldnumsz(j)+h;
    end
    
    Ptot(i,:)=Ptot(i)+18*[sum(exP(:,i)) sum(eyP(:,i)) sum(ezP(:,i))];
    Ptot(i,:)=Ptot(i,:)/(60*18);
    Polymag(i)=sqrt(Ptot(i,1)^2+Ptot(i,2)^2+Ptot(i,3)^2);
end
    
% Pave(k)=mean(Polymag);
% Nave(k)=mean(Nanomag);
Pave(k)=sqrt(sum(Polymag.^2)/length(Polymag));
Nave(k)=sqrt(sum(Nanomag.^2)/length(Nanomag));
Polymagout(k,:)=Polymag;
Nanomagout(k,:)=Nanomag;

end

XiNano
XiPoly
mNave=mean(Nave)
mPave=mean(Pave)
saveaves(N,:)=[mNave mPave]

caveN=cum_ave(Nave);
caveP=cum_ave(Pave);

% plot(1:numruns,caveN)
% hold on
% plot(1:numruns,caveP)

%% Plotting
% plot(1:totaltime,Nanomag)
% hold on
% plot(1:totaltime,Polymag)

% profile viewer

end

function output=cum_ave(input);
    for i=1:length(input)
        output(i)=sum(input(1:i))/i;
    end
end






% 
% 
% 
% 
% 
% 
% figure
% for i=1:length(system.attype)
%     if system.attype(i)=='A'
%         colors(i,:)=[0 0 0];
%         size(i)=100;
%     elseif system.attype(i)=='B'
%         colors(i,:)=[1 0 0];
%         size(i)=30;
%     elseif system.attype(i)=='C'
%         colors(i,:)=[0 0 1];
%         size(i)=10;
%     elseif system.attype(i)=='D'
%         colors(i,:)=[0 1 0];
%         size(i)=10;
%     end
% end
% scatter3(system.pos(:,1),system.pos(:,2),system.pos(:,3),size,colors)

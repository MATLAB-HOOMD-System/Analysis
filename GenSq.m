function [rs,grs]=GenSq(path,varargin)


p=inputParser;
defaultfilename='test.gsd';
defaultsinglepath=0;
defaultplotGR=1;
defaultsaveGR=1;
defaultplotSQ=1;
defaultsaveSQ=1;
defaultlabelqstar=0;
defaultplotQmax=1;
defaultsaveQmax=1;
defaultplotD=1;
defaultsaveD=1;
defaultcloseplots=1;
defaultSelections1={'name D'};
defaultSelections2='default';
defaultStepSizeTime=1;
defaultcustomaxis=[];
defaultXmlStartidIntifierLength=9;
defaultDelta=0.1;
defaultFirstStep=1; %if <=0 then relative to last step
defaultLastStep=0; %if <=0 then relative to last step
defaultStepSize=1;
defaultOverwriteFiles=0;
defaultShowPlots=0;

% path='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Refine_PCND_Nano/TestXi0result/testbestsetsagain/';
filename='PCND_Nano_XiNano0.02_tNano100000_XiPoly0_tPoly100000_longhalf_start.xml';
singlepath=0;%0=singlefile 1=singlepath 2=allpaths
plotGR=1;
saveGR=1;
plotSQ=1;
saveSQ=1;
labelqstar=0;
plotQmax=1;
saveQmax=1;
plotD=1;
saveD=1;
closeplots=1;
Selections1={'name D'};
Selections2=Selections1;
StepSizeTime=1;
customaxis=[];
XmlStartidIntifierLength=9;
Delta=0.1;
FirstStep=1; %if <=0 then relative to last step
LastStep=0; %if <=0 then relative to last step
StepSize=1;
OverwriteFiles=0;
ShowPlots=0;

addParameter(p,'filename',defaultfilename)
addParameter(p,'singlepath',defaultsinglepath,@isnumeric)
addParameter(p,'plotGR',defaultplotGR,@isnumeric)
addParameter(p,'saveGR',defaultsaveGR,@isnumeric)
addParameter(p,'plotSQ',defaultplotSQ,@isnumeric)
addParameter(p,'saveSQ',defaultsaveSQ,@isnumeric)
addParameter(p,'labelqstar',defaultlabelqstar,@isnumeric)
addParameter(p,'plotQmax',defaultplotQmax,@isnumeric)
addParameter(p,'saveQmax',defaultsaveQmax,@isnumeric)
addParameter(p,'plotD',defaultplotD,@isnumeric)
addParameter(p,'saveD',defaultsaveD,@isnumeric)
addParameter(p,'closeplots',defaultcloseplots,@isnumeric)
addParameter(p,'Selections1',defaultSelections1)
addParameter(p,'Selections2',defaultSelections2)
addParameter(p,'StepSizeTime',defaultStepSizeTime,@isnumeric)
addParameter(p,'customaxis',defaultcustomaxis,@isnumeric)
addParameter(p,'XmlStartidIntifierLength',defaultXmlStartidIntifierLength,@isnumeric)
addParameter(p,'Delta',defaultDelta,@isnumeric)
addParameter(p,'FirstStep',defaultFirstStep,@isnumeric)
addParameter(p,'LastStep',defaultLastStep,@isnumeric)
addParameter(p,'StepSize',defaultStepSize,@isnumeric)
addParameter(p,'OverwriteFiles',defaultOverwriteFiles,@isnumeric)
addParameter(p,'ShowPlots',defaultShowPlots,@isnumeric)

parse(p,varargin{:});
fields=fieldnames(p.Results);
for i=1:numel(fields)
    eval([fields{i} '=' 'p.Results.' fields{i} ';']);
    if strcmp(fields{i},'Selections2')
        if strcmp(Selections2,defaultSelections2)
            Selections2=Selections1;
        end
    end
end

if ShowPlots==0
    set(0, 'DefaultFigureVisible', 'off');
end


% filename='PCND_Nano_XiNano0.02_tNano100000_XiPoly0_tPoly100000_longhalf_start.xml';
% singlepath=0;%0=singlefile 1=singlepath 2=allpaths
% plotGR=1;
% saveGR=1;
% plotSQ=1;
% saveSQ=1;
% labelqstar=0;
% plotQmax=1;
% saveQmax=1;
% plotD=1;
% saveD=1;
% closeplots=1;
% Selections1={'name D','name A'};
% Selections2=Selections1;
% StepSizeTime=1;
% customaxis=[];
% XmlStartidIntifierLength=9;
% Delta=0.1;
% FirstStep=1; %if <=0 then relative to last step
% LastStep=0; %if <=0 then relative to last step
% StepSize=1;
% OverwriteFiles=1;


%% Find files and run vmd
if length(Selections1)~=length(Selections2)
    error('Selections not of equal size');
end
close all

if singlepath==2
    path=genpath(path);
    paths = split(path,':');

    for i=1:length(paths)-1
        newpath=[paths{i} '/'];
        files=dir(newpath);
        [r,gr,numtimesteps,Outfiles,rs,grs]=localtestfiles()
    end
else

    paths=dir(path);
    newpath=path;
    files=dir(newpath);
    [r,gr,numtimesteps,Outfiles,rs,grs]=localtestfiles()
end


function [r,gr,numtimesteps,Outfiles,rs,grs]=localtestfiles()
% %% bring in all variables
% ALLVAR = evalin('base','whos');
% for ii = 1:length(ALLVAR)
%    C_ =  evalin('base',[ALLVAR(ii).name ';']);
%    eval([ALLVAR(ii).name,'=C_;']);
% end
% clear ALLVAR C_ ii

% for j=3:length(files)
% %     disp('j=');disp(j);
%     startIndex = regexp(files(j).name,'PCND_Nano.+start.xml');
%     if (singlepath==0 && strcmp(filename,files(j).name)) || (singlepath~=0 && ~isempty(startIndex))
%         disp('j=');disp(j);
%         files(j).name
%         pause()
%     end
% end



for j=3:length(files)
    disp('j=');disp(j);
    files(j).name
    startIndex = regexp(files(j).name,'PCND_Nano.+start.xml');
    if (singlepath==0 && strcmp(filename,files(j).name)) || (singlepath~=0 && ~isempty(startIndex))
        xmlname=files(j).name;
        dcdname=[xmlname(1:end-XmlStartidIntifierLength) 'PCND1traj.dcd'];
        Doutnametest=[[xmlname(1:end-XmlStartidIntifierLength) '_D.jpg']];
        if ((exist([newpath dcdname], 'file') == 2) && (OverwriteFiles==0 || exist([newpath Doutnametest],'file')~=2) && (exist([newpath xmlname(1:end-XmlStartidIntifierLength) 'qmax1.mat'],'file')==0 || OverwriteFiles==1))
            h=read_dcdheader([newpath dcdname]);
            nsteps=h.NSET;
            calcflag=1; %check to make sure there are enough time steps
            if abs(FirstStep)>nsteps || abs(LastStep)>nsteps
                calcflag=0;
            end
            if FirstStep<=0
                FStep=nsteps+FirstStep;
            else
                FStep=FirstStep;
            end
            if LastStep<=0
                LStep=nsteps+LastStep;
            else
                LStep=LastStep;
            end
            
            
            Inputnames={[newpath xmlname],[newpath dcdname]};
            for l=1:length(Selections1)
                Outfiles{l}=[newpath xmlname(1:end-XmlStartidIntifierLength) Selections1{l} ':' Selections2{l} '.dat'];
            end
            if calcflag==1
                [r,gr,numtimesteps] = rdf('StepSizeTime',StepSizeTime,'Inputnames',Inputnames,'Selection1',Selections1,'Selection2',Selections2,'Delta',Delta,'Rmax',100,'FirstStep',FStep,'LastStep',LStep,'StepSize',StepSize,'OutFile',Outfiles);              
                localplotting(r,gr,numtimesteps,Outfiles,xmlname,dcdname,nsteps);
                if exist('rs','var')
                    rs=[rs,r];
                    gr=[grs,gr];
                else
                    rs=r;
                    grs=gr;
                end

            end
        end
    end
end



end

function localplotting(r,gr,numtimesteps,Outfiles,xmlname,dcdname,nsteps)
%% bring in all variables
% ALLVAR = evalin('base','whos');
% for ii = 1:length(ALLVAR)
%    C_ =  evalin('base',[ALLVAR(ii).name ';']);
%    eval([ALLVAR(ii).name,'=C_;']);
% end
% clear ALLVAR C_ ii

%% Plotting of GR
colors=lines(numtimesteps);
counter=1;

if plotGR==1
    for i=1:length(Outfiles)
        for k=1:numtimesteps
            if k==1
                figure
                plot(r(:,counter), gr(:,counter), 'Color',colors(k,:), 'LineWidth', 2);
                title({'Radial Pair Distribution Function g(r)', [Selections1{i} ':' Selections2{i}]});
                axis([0 max(r(:,counter)) 0 max(gr(:,counter))+0.5]);
                if customaxis~=[]
                    axis(customaxis);
                end
                xlabel('r');
                ylabel('g(r)');
                grid on;
                hold on
            else
                plot(r(:,counter), gr(:,counter), 'Color',colors(k,:), 'LineWidth', 2);
            end
            counter=counter+1;
        end
        fpath_AA_gr = [newpath xmlname(1:end-XmlStartidIntifierLength) 'g(r)_' Selections1{i} ':' Selections2{i} '.jpg'];
        if saveGR==1;
            saveas(gcf, fpath_AA_gr);
        end
    end
end

%% Calculation of S(q)  and q* and D          
counter=1;
if plotSQ==1;
    for i=1:length(Outfiles)
        for k=1:numtimesteps
            %%%%%%%%%%%%%%%%%
            rAA=r(:,counter);
            grAA=gr(:,counter);
            rmax_AA = rAA(end);
            ru0_AA = 1;
            n1 = 1;
            for m1 = 1:length(rAA)
                if m1 == 1
                    del_r_AA(1,1) = rAA(1,1) - 0;
                else
                    del_r_AA(m1,1) = rAA(m1,1) - rAA(m1-1,1);
                end
            end
            qs_AA = 0.001:0.001:10;
            for q1 = qs_AA
                S_AA = 4.*pi.*ru0_AA.*rAA.*((sin(q1.*rAA))./q1).*(grAA-1).*(sin((pi.*rAA)./rmax_AA)./((pi.*rAA)/rmax_AA));
                Sq_AA(n1,1) = (sum(S_AA(1:end).*del_r_AA))-1;
                n1 = n1 + 1;
            end
            Sq(:,counter)=Sq_AA;
            q(:,counter)=qs_AA;
            z_AA = find(Sq_AA == max(Sq_AA));
            q_star_AA = qs_AA(z_AA);
            Lm_AA=2*pi()/q_star_AA;
            E_AA=0.142*(Lm_AA/rmax_AA)^(1.92);
            D_AA=Lm_AA/(E_AA+1);
            qstar(counter)=q_star_AA;
            D(counter)=D_AA;
            qmax(counter)=max(Sq_AA);
            %%%%%%%%%%%%%%%%%%%%%%

            %%%%%%%%%%%%plot
            if k==1
                figure
                plot(q(:,counter), Sq(:,counter), 'Color',colors(k,:), 'LineWidth', 2);
                if labelqstar==1
                    text(q_star_AA, Sq_AA(z_AA), ['\leftarrow q* = ' num2str(q_star_AA) '    D = ' num2str(D_AA)]);
                end
                title({'Structure Factor S(q)', [Selections1{i} ':' Selections2{i}]});
                xlabel('q');
                ylabel('S(q)');
                if customaxis~=[]
                    axis(customaxis);
                end
                grid on;
                hold on
            else
                plot(q(:,counter), Sq(:,counter), 'Color',colors(k,:), 'LineWidth', 2);
                if labelqstar==1
                    text(q_star_AA, Sq_AA(z_AA), ['\leftarrow q* = ' num2str(q_star_AA) '    D = ' num2str(D_AA)]);
                end
            end
            counter=counter+1;
        end
        fpath_AA_gr = [newpath xmlname(1:end-XmlStartidIntifierLength) 'SQ(r)_' Selections1{i} ':' Selections2{i} '.jpg'];
        if saveSQ==1;
            saveas(gcf, fpath_AA_gr);
        end
    end
end



%% D vs time
colors=lines(length(Outfiles));
if plotD==1
    if plotSQ~=1
        error('didn''t calculate Sq yet');
    end
    for i=1:length(Outfiles)
        if i==1
            figure
            xplot=1:numtimesteps;
            yplot=D(((i-1)*numtimesteps)+1:i*numtimesteps);
            plot(1:numtimesteps,D(((i-1)*numtimesteps)+1:i*numtimesteps),'Color',colors(i,:),'LineWidth', 2);
            title({'D'});
            xlabel('t');
            ylabel('D');
            grid on;
            hold on;
            save([newpath xmlname(1:end-XmlStartidIntifierLength) 'Domain' num2str(i) '.mat'],'xplot','yplot');
        else
            xplot=1:numtimesteps;
            yplot=D(((i-1)*numtimesteps)+1:i*numtimesteps);
            plot(1:numtimesteps,D(((i-1)*numtimesteps)+1:i*numtimesteps),'Color',colors(i,:),'LineWidth', 2);
            save([newpath xmlname(1:end-XmlStartidIntifierLength) 'Domain' num2str(i) '.mat'],'xplot','yplot');
        end
    end
    %make legend text
    for l=1:length(Outfiles)
        legendtext{l}=[Selections1{l} ':' Selections2{l}];
    end
    legend(legendtext,'Location','best');
    fpath_AA_gr = [newpath xmlname(1:end-XmlStartidIntifierLength) 'D' '.jpg'];
    if saveD==1;
        saveas(gcf, fpath_AA_gr);
    end
    if evalin('base','exist(''Ds'',''var'')')
        Ds=evalin('base','Ds');
        x=Ds;
        y=D';
        sx = size(x);
        sy = size(y);
        a = max(sx(1),sy(1))
        z = [[x;zeros(abs([a 0]-sx))],[y;zeros(abs([a,0]-sy))]]
        Ds=z;     
        assignin('base','Ds',Ds);
    else
        Ds=[D'];
        assignin('base','Ds',Ds);
    end
end
%% qmax vs time
colors=lines(length(Outfiles));
if plotQmax==1
    if plotSQ~=1
        error('didn''t calculate Sq yet');
    end
    for i=1:length(Outfiles)
        if i==1
            figure
            xplot=1:numtimesteps;
            yplot=qmax(((i-1)*numtimesteps)+1:i*numtimesteps);
            plot(1:numtimesteps,qmax(((i-1)*numtimesteps)+1:i*numtimesteps),'Color',colors(i,:),'LineWidth', 2);
            title({'q_{max}'});
            xlabel('t');
            ylabel('q_{max}');
            grid on;
            hold on;
            save([newpath xmlname(1:end-XmlStartidIntifierLength) 'qmax' num2str(i) '.mat'],'xplot','yplot');
        else
            xplot=1:numtimesteps;
            yplot=qmax(((i-1)*numtimesteps)+1:i*numtimesteps);
            plot(1:numtimesteps,qmax(((i-1)*numtimesteps)+1:i*numtimesteps),'Color',colors(i,:),'LineWidth', 2);
            save([newpath xmlname(1:end-XmlStartidIntifierLength) 'qmax' num2str(i) '.mat'],'xplot','yplot');
        end
    end
%make legend text
    for l=1:length(Outfiles)
        legendtext{l}=[Selections1{l} ':' Selections2{l}];
    end
    legend(legendtext,'Location','best');
    fpath_AA_gr = [newpath xmlname(1:end-XmlStartidIntifierLength) 'qmax' '.jpg'];
    if saveQmax==1;
        saveas(gcf, fpath_AA_gr);
    end
    
    if evalin('base','exist(''qmaxs'',''var'')')
        qmaxs=evalin('base','qmaxs');
        x=qmaxs;
        y=qmax';
        sx = size(x);
        sy = size(y);
        a = max(sx(1),sy(1))
        z = [[x;zeros(abs([a 0]-sx))],[y;zeros(abs([a,0]-sy))]]
        qmaxs=z;
        assignin('base','qmaxs',qmaxs);
    else
        qmaxs=[qmax'];
        assignin('base','qmaxs',qmaxs);
    end
end



if closeplots==1
    close all
end


fclose all
end
set(0, 'DefaultFigureVisible', 'on');
end
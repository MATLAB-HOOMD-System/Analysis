function [distances,squareformdistances,x,y,z]=MinimumDistance(dim,pos1,pos2)
%calculates the minimum distance for all positions in pos 1 if only pos1 is
%defeined, and for all pairs of pos1 with pos2 if pos1 and pos2 are
%defined.

if exist('pos2','var')==0
    pos=pos1;
    %xdistances
    distances=(pdist(pos(:,1)));
    distances(distances>dim(1)/2)=dim(1)-distances(distances>dim(1)/2);
    distances=distances.^2;

    % disp('after x distances')
    % showmemorytotal

    %ydistances
    tempdistances=(pdist(pos(:,2)));
    tempdistances(tempdistances>dim(2)/2)=dim(2)-tempdistances(tempdistances>dim(2)/2);
    distances=distances+tempdistances.^2;

    % disp('after y distances')
    % showmemorytotal

    %zdistances
    tempdistances=(pdist(pos(:,3)));
    tempdistances(tempdistances>dim(3)/2)=dim(3)-tempdistances(tempdistances>dim(3)/2);
    distances=distances+tempdistances.^2;

    distances=distances.^(1/2);
    squareformdistances=squareform(distances);
else
    %x
    distances=pdist2(pos1(:,1),pos2(:,1));
    distances(distances>dim(1)/2)=dim(1)-distances(distances>dim(1)/2);
    x=distances;
    distances=distances.^2;
    
    %y
    tempdistances=pdist2(pos1(:,2),pos2(:,2));
    tempdistances(tempdistances>dim(2)/2)=dim(2)-tempdistances(tempdistances>dim(2)/2);
    y=tempdistances;
    distances=distances+tempdistances.^2;
    
    %z
    tempdistances=pdist2(pos1(:,3),pos2(:,3));
    tempdistances(tempdistances>dim(3)/2)=dim(3)-tempdistances(tempdistances>dim(3)/2);
    z=tempdistances;
    distances=distances+tempdistances.^2;
    
    distances=distances.^(1/2);
    squareformdistances=distances;
    distances=distances(1:size(distances,1)*size(distances,2));
end
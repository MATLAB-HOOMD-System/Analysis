
function [xyz0,xyzs,t,xl,yl,zl,x1,y1,z1,a1,b1,c1]=linfit3d(xyz)

    if size(xyz,1)~=3
        xyz=xyz';
        if size(xyz,1)~=3
            error('points are the wrong size')
        end
    end

    xyz0 = mean(xyz,2);
    A = xyz-xyz0;
    [U,S,~] = svd(A);
    d = U(:,1);
    xyzs=d;
    
    t = d'*A;
    t1 = min(t);
    t2 = max(t);
    xzyl = xyz0 + [t1,0,t2].*d; % size 3x2

    xl = xzyl(1,:);
    yl = xzyl(2,:);
    zl = xzyl(3,:);
    
    x1=xl(1);
    y1=yl(1);
    z1=zl(1);
    
    a1=xl(2)-x1;
    b1=yl(2)-y1;
    c1=zl(2)-z1;



    %% code from online I took to make it
    % Fake n-points in R3, n=4 in your case
    %     n = 10;
    %     a = randn(3,1);
    %     b = randn(3,1);
    %     t = rand(1,10);
    %     tstart=t;
    %     xyz = a + b.*t;
    %     xyz2=xyz;
    %     xyz = xyz + 0.05*randn(size(xyz)); % size 3 x n
    %     % Engine  
    %     xyz0 = mean(xyz,2);
    %     A = xyz-xyz0;
    %     [U,S,~] = svd(A);
    %     d = U(:,1);
    %     t = d'*A;
    %     t1 = min(t);
    %     t2 = max(t);
    %     xzyl = xyz0 + [t1,0,t2].*d; % size 3x2
    % 
    %     % Check
    %     x = xyz(1,:);
    %     y = xyz(2,:);
    %     z = xyz(3,:);
    %     xl = xzyl(1,:);
    %     yl = xzyl(2,:);
    %     zl = xzyl(3,:);
    %     close all
    %     hold on
    %     plot3(x,y,z,'o');
    %     plot3(xl,yl,zl,'ro-');
    %     axis equal
end
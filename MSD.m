function [MSD,Ms]=MSD(pos,dimsize)
debug=1;
if size(pos,2)~=3
    error('position array not correct size. Must be Mx3 array')
end
if size(pos,3)==1
    error('must inlcude multiple timesteps')
end

for i=1:size(pos,1);
    for j=2:size(pos,3)
        for d=1:3
            test=1;
            while test==1
                if pos(i,d,j)-pos(i,d,j-1)>dimsize(d)/2
                    pos(i,d,j)=pos(i,d,j)-dimsize(d);
                elseif pos(i,d,j)-pos(i,d,j-1)<-dimsize(d)/2
                    pos(i,d,j)=pos(i,d,j)+dimsize(d);
                else
                    test=0;
                end
            end
        end
    end
end





output=zeros(1,size(pos,3));

InitialPos=pos(:,:,1);

for t=1:size(pos,3)
    M=0;
    for i=1:size(pos,1)
        M2=0;
        for x=1:3
            M=M+(pos(i,x,t)-pos(i,x,1))^2;
            M2=M2+(pos(i,x,t)-pos(i,x,1))^2;
        end
        if debug==1
            Ms(t,i)=M2;
        end
    end
    
    MSD(t)=M/size(pos,1);
    if debug==0
        Ms=Nan;
    end
end
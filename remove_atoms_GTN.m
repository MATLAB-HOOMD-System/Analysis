function system=remove_atoms_GTN(system,N)


% N=682;
systemBB=system;
systemBB.natoms=N;
systemBB.attype=systemBB.attype(1:N);
systemBB.mass=systemBB.mass(1:N);

test=1;
i=1;
while test==1
    if systemBB.bonds(i,1)>N-1 || systemBB.bonds(i,2)>N-1
        systemBB.bonds(i,:)=[];
    else
        i=i+1;
    end
    if i>size(systemBB.bonds)
        test=0;
    end
end



test=1;
i=1;
while test==1
    if systemBB.angles(i,1)>N-1 || systemBB.angles(i,2)>N-1 || systemBB.angles(i,3)>N-1
        systemBB.angles(i,:)=[];
    else
        i=i+1;
    end
    if i>size(systemBB.angles)
        test=0;
    end
end


system=systemBB;
function [r gofr]=vmd_gofr(path,filename,rmax,dr)
% Path from Matlab folder to file in question
% Filename of xml to be analyzed. Do not include .xml.
% path='Test_Phase_Identification/Clear_Big_Phases/';
% filename='Mixed_To_Ordered_NPT_epsAB=0.309_numA=3_numB=21_final';
% rmax=45;
% dr=0.1;

if ~strcmp(filename(end-3:end),'.xml')
    filename=[filename '.xml'];
end
if ~strcmp(path(end),'/')
    path=[path '/'];
end
if strcmp(path(1:4),'C:/U')
    path=path(36:end);
end

fid=fopen('~/matlab/vmd_gofr_cmd.tcl','w','l');
% fprintf(fid,['mol new ~/' path filename ' type hoomd\n']);
printf(fid,['mol new ' filename ' type hoomd\n']);
fprintf(fid,['set gr_n ~/' path filename '.dat\n']);
fprintf(fid,['set sel1 [atomselect top "name A"];set sel2 [atomselect top "name A"]\n']);
fprintf(fid,['set gr [measure gofr $sel1 $sel2 delta ' num2str(dr) ' rmax ' num2str(rmax) ' usepbc 1 selupdate 0 first 0 last -1 step 1]\n']);
fprintf(fid,['set outfile [open $gr_n w]\n']);
fprintf(fid,['set r [lindex $gr 0]\n']);
fprintf(fid,['set gr2 [lindex $gr 1];set igr [lindex $gr 2];\n']);
fprintf(fid,['set i 0\n']);
fprintf(fid,['foreach j $r k $gr2 l $igr {puts $outfile "$j $k $l"}\n']);
fprintf(fid,['close $outfile\n']);
fprintf(fid,['exit\n']);
fclose(fid);


% winopen('Mixed_To_Ordered_NPT_epsAB=0.3_numA=2_numB=14_final.xml')

% system(['"C:\Program Files (x86)\University of Illinois\VMD\vmd.exe" C:/Users/bnation3/Documents/MATLAB/Test_Phase_Identification/Clear_Big_Phases/Mixed_To_Ordered_NPT_epsAB=0.309_numA=3_numB=21_final.xml -args set gr_n C:/Users/bnation3/Documents/MATLAB/Test_Phase_Identification/Clear_Big_Phases/Mixed_To_Ordered_NPT_epsAB=0.309_numA=3_numB=21_final.dat; set sel1 [atomselect top "name A"]; set sel2 [atomselect top "name A"]; set gr [measure gofr $sel1 $sel2 delta 0.01 rmax 45 usepbc 1 selupdate 0 first 0 last -1 step 1]; set outfile [open $gr_n w]; set r [lindex $gr 0]; set gr2 [lindex $gr 1]; set igr [lindex $gr 2]; set i 0; foreach j $r k $gr2 l $igr {puts $outfile "$j $k $l"}; close $outfile; mol delete all; exit')%
system('vmd -e ~/matlab/vmd_gofr_cmd.tcl')

data=load(['~/' path filename '.dat'],'-ascii');
r=data(:,1);
gofr=data(:,2);

end
